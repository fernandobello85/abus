/***************************/
/**   Projet SRP - aBuS   **/
/***************************/
/**     Wetzel Anthony    **/
/**	    Bello Fernando    **/
/***************************/

/* Package */
package abus_applet;

/* Import */
import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.OwnerPIN;
import javacard.framework.Util;
import java.nio.ByteBuffer;
import java.io.*;
import java.util.Date;

public class abusApplet extends Applet {
	
	/************************** Déclaration des constantes **************************/
	
	/* Instructions d'entrée */
	public static final byte CLA_ABUSAPPLET	= (byte)  0x25;	
	public static final byte INS_RECHARGEMENT = 0x00;
	public static final byte INS_ACHAT = 0x01;
	public static final byte INS_CONS_CREDIT = 0x02;
	public static final byte INS_CONS_VOGAYE_VALIDE = 0x03;
	public static final byte INS_DEBLOQUER_PIN = 0x04;
	public static final byte INS_CONS_ACTIVITE = 0x05;
	
	/* Paramètres de la carte */
	public static final byte BALANCE_MAX_AUTORISE = 100;
	public static final byte MONTANT_MAX_RECHARGEMENT = 100;
	public static final short DUREE_VALIDITE_VOYAGE = 60;
	
	public static final byte NB_TENTATIVES_PIN = (byte) 0x03; // maximum PIN tries
	public static final byte NB_TENTATIVES_PUK = (byte) 0x03; // maximum PUK tries 
	public final static byte MAX_PIN_SIZE =(byte) 0x02; 	// maximum size PIN
	public final static byte MAX_PUK_SIZE =(byte) 0x02; 	// maximum size PUK
	
	/* Messages de l'Applet */
	final static short SW_VERIFICATION_FAILED = 0x6300; // signal that PIN verification failed
	final static short SW_INVALID_TRANSACTION_AMOUNT = 0x6A83; // amount > MAX_TRANSACTION_AMOUNT or amount < 0
    final static short SW_EXCEED_MAXIMUM_BALANCE = 0x6A84; // signal that balance exceed the maximum
    final static short SW_EXCEED_SOLDE_MINIMUM = 0x6A82; // signal the solde is minimum
    final static short SW_PIN_BLOCKED = (short) 0x6983; // signal that pin is blocked
    final static short SW_PUK_BLOCKED = (short) 0x6984; // signal that puk is blocked
    final static short SW_OPERATION_NOT_ALLOWED = (short) 0x6400; // signal that operation is not allowed
    final static short SW_INCORRECT_P1 = (short) 0x6401; // signal that parameter in P1 is not correct
    final static short SW_TIME_TRIP_EXCEED = (short) 0x6100; // signal that time trip is exceeded
    final static short SW_WRONG_LENGTH = (short) 0x6200; // Signal that number of output data is not correct
    
	/* Données internes */
	private byte balance;
	private OwnerPIN pin;
	private OwnerPIN puk;
	private byte monthCard;
	private short dayCard;
	private short yearCard;
	private short timeCard;
	private byte lineCard;
	private boolean directionCard;
	//private short journalTab[] = { (short) 0x31fcffff59f64ff1L, (short) 0x101107e230c010L, (short) 0x201107e232a020L, (short) 0x30a207e24ec64L }; // Données trop grandes pour un short
	
	/* Données externes */
	private byte monthCurrent;
	private short dayCurrent;
	private short yearCurrent;
	private short timeCurrent;
	private byte lineCurrent;
	private boolean directionCurrent;
	private byte montantRecharger;
	
	/************************** Application **************************/
	
	/* Instanciation des variables de l'application */
	private abusApplet() {
		balance = 0;
		monthCard = 12;
		dayCard = 30;
		yearCard = 2018;
		timeCard = 1000;
		pin = new OwnerPIN(NB_TENTATIVES_PIN, MAX_PIN_SIZE); // PIN creation (size & tentatives)
		puk = new OwnerPIN(NB_TENTATIVES_PUK, MAX_PUK_SIZE); // PUK creation (size & tentatives)
		pin.update(new byte[] {0x12, 0x34}, (short) 0, MAX_PIN_SIZE); // PIN Code definition
		puk.update(new byte[]{0x00, 0x00}, (short) 0, MAX_PUK_SIZE); // PUK Code definition
	}

	public static void install(byte bArray[], short bOffset, byte bLength)
			throws ISOException {
		new abusApplet().register();
	}
	
	/**
	 * Fonction permettant de vérifier que le pin de la carte n'est pas bloqué avant commencer
	 * @return boolean - True if not blocked False if blocked
	 */
	public boolean select() {
	    // The applet declines to be selected if the pin is blocked.
	    if(pin.getTriesRemaining() == 0) return false;
	    return true;
	} // end of select method
	
	/** 
	 * Fonction permettant de recharger la carte
	 * @param apdu - APDU
	 * @param buffer - montant du rechargement
	 */
	private void rechargeCard(APDU apdu, byte[] buffer) {
		byte pin_nb = buffer[ISO7816.OFFSET_P1];
		if(sizeP1Paswd(pin_nb, MAX_PIN_SIZE)) {
	   		ISOException.throwIt(SW_INCORRECT_P1); // PIN size error
	   	}
		if(pin.getTriesRemaining() == 0) {
			ISOException.throwIt(SW_PIN_BLOCKED); // PIN blocked
			return;
			}
	   	if(pin.check(buffer, ISO7816.OFFSET_CDATA, MAX_PIN_SIZE) == false ) {
	   		ISOException.throwIt((short) ((SW_VERIFICATION_FAILED)+(short) pin.getTriesRemaining())); // Check PIN incorrect
	   	} else {
	   		//System.out.println(buffer[ISO7816.OFFSET_CDATA+2]);
	   		if(BALANCE_MAX_AUTORISE < balance + buffer[ISO7816.OFFSET_CDATA+2]) {
	   			ISOException.throwIt(SW_EXCEED_MAXIMUM_BALANCE); // Balance exceeded
	   		} else if(MONTANT_MAX_RECHARGEMENT < buffer[ISO7816.OFFSET_CDATA+2] || buffer[ISO7816.OFFSET_CDATA+2] < 0) {
	   			ISOException.throwIt(SW_INVALID_TRANSACTION_AMOUNT); // Recharge amount exceeded
	   		} else {
	   			montantRecharger = buffer[ISO7816.OFFSET_CDATA+2];// Sauvegarder le montant du dernière rechargement
	   			balance = (byte) (balance + montantRecharger);
	   			// Si jamais on veut que cette fonction retourne le nouveau balance de la carte
	   			//buffer[0]  = balance;
	   			//apdu.setOutgoingAndSend((short) 0, (short) 1);
	   		}
	   	}
	   	apdu.setOutgoingAndSend((short) 0, (short) 1);
	}
	
	/** 
	 * Fonction permettant de vérifier que la taille du PIN est correcte
	 * @param pin_nb et size_local_pwd
	 * @param boolean - True if correct False Incorrect
	 */
	private boolean sizeP1Paswd(byte pin_nb, byte size_local_pwd) {
		//PIN validation 
		return (pin_nb < 0) || (pin_nb > size_local_pwd);
	}
	
	/**
	 * Fonction permettant de débloquer le code PIN
	 * @param apdu - APDU
	 * @param buffer - saisie du code PUK
	 */
	private void UnblockPIN(APDU apdu, byte[] buffer) {
		byte pin_nb = buffer[ISO7816.OFFSET_P1];

		if(puk.getTriesRemaining() == 0) {
			ISOException.throwIt(SW_PUK_BLOCKED); // PUK blocked
			return;
		}
		if(puk.check(buffer, ISO7816.OFFSET_CDATA, MAX_PUK_SIZE) == false ) {
			ISOException.throwIt((short) ((SW_VERIFICATION_FAILED)+(short) puk.getTriesRemaining())); // Check PUK incorrect
		}
		if(sizeP1Paswd(pin_nb, MAX_PUK_SIZE)) {
			ISOException.throwIt(SW_INCORRECT_P1); // PUK size error
		}
		/*
		if(pin == null) {
			ISOException.throwIt(SW_INCORRECT_P1);
		}
		if(ublk_pin == null) {
			ISOException.throwIt(SW_INTERNAL_ERROR);
		}*/
		
		// If the PIN is not blocked, the call is inconsistent

		if(pin.getTriesRemaining() != 0) {
			ISOException.throwIt(SW_OPERATION_NOT_ALLOWED); // PIN is not blocked
		}
		/*if(buffer[ISO7816.OFFSET_P2] != 0x00) {
			ISOException.throwIt(SW_INCORRECT_P2);
		}*/
		
		short numBytes = Util.makeShort((byte) 0x00, buffer[ISO7816.OFFSET_LC]);
		if(numBytes != apdu.setIncomingAndReceive()) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH); // Size PUK in error
		}
		pin.resetAndUnblock();		
	}
	
	/**
	 * ?
	 * @param apdu - ?
	 * @param buffer - ?
	 */
	public void validationVayage(APDU apdu, byte[] buffer) {
		monthCurrent = buffer[ISO7816.OFFSET_CDATA+0];
		dayCurrent = buffer[ISO7816.OFFSET_CDATA+1];
		yearCurrent = ByteBuffer.wrap(buffer,buffer[ISO7816.OFFSET_CDATA+2],2).getShort();
		timeCurrent = ByteBuffer.wrap(buffer,9,2).getShort(); // Exprimé en minutes 
		
		short[] tempsEcoule = tempsEcoule(yearCurrent, monthCurrent, dayCurrent, timeCurrent);

		System.out.print("Temps ecoule retourne mm/jj/aaaa minutes:"); // Seulement pour le test
		for(byte i=0; i< tempsEcoule.length; i++) {
			System.out.print(tempsEcoule[i]+" ");
		}
		System.out.println("");
		
		if(tempsEcoule[0] > 0 || tempsEcoule[1] > 0 || tempsEcoule[2] > 0) {
			System.out.println("Voyage terminé. Durée dépassement: "+tempsEcoule[0]+
					" mois "+tempsEcoule[1]+" jours "+tempsEcoule[2]+" années"); // Seulement pour le test
			buffer[0]  = (byte) 0; // 0 if time exceeded
			buffer[1]  = (byte) tempsEcoule[0]; //Month
			buffer[2]  = (byte) tempsEcoule[1]; //Day
			buffer[3]  = (byte) tempsEcoule[2]; //Year
			buffer[4] = (byte)(0); //Time 0
		    buffer[5] = (byte)(0); //Time 1
   			apdu.setOutgoingAndSend((short) 0, (short) 6);
		}
		if(tempsEcoule[3] > 60) {
			System.out.println("Voyage terminé. Durée dépassement: "+(tempsEcoule[3]-60)+" minutes"); // Seulement pour le test
			buffer[0]  = (byte) 0; //0 if time exceeded
			buffer[1]  = (byte) tempsEcoule[0]; //Month
			buffer[2]  = (byte) tempsEcoule[1]; //Day
			buffer[3]  = (byte) tempsEcoule[2]; //Year
			buffer[4] = (byte)(tempsEcoule[3]-60 >> 8); //Time byte 0
		    buffer[5] = (byte)(tempsEcoule[3]-60 & 0xFF); //Time byte 1
   			apdu.setOutgoingAndSend((short) 0, (short) 6);
		}
		if(tempsEcoule[0] == 0 && tempsEcoule[1] == 0 && tempsEcoule[2] == 0 && tempsEcoule[3] < 61) {
			System.out.println("Voyage encore valide. Temps restant : "+(60 - tempsEcoule[3])); // Seulement pour le test
			buffer[0]  = (byte) 1; //1 if time is not exceeded
			buffer[1]  = (byte) tempsEcoule[0]; //Month
			buffer[2]  = (byte) tempsEcoule[1]; //Day
			buffer[3]  = (byte) tempsEcoule[2]; //Year
			buffer[4] = (byte)(60 - tempsEcoule[3] >> 8); //Time byte 0
		    buffer[5] = (byte)(60 - tempsEcoule[3] & 0xFF);//Time byte 1
   			apdu.setOutgoingAndSend((short) 0, (short) 6);
		}
	}
	
	/**
	 * Fonction permettant de calculer le temps écoulé entre deux dates
	 * @param year - année du dernier achat
	 * @param month - mois du dernier achat
	 * @param day - jour du dernier achat
	 * @param time - heure du dernier achat
	 * @return short - Différence de temps entre les deux dates
	 */
	public short[] tempsEcoule(short year, byte month, short day, short time) {
		// Saisie des variables par l'utilisateur
		// yearCurrent
		// monthCurrent
		// dayCurrent
		// timeCurrent
		
		System.out.println("Dans la fonction tempsEcoule");
		System.out.println("Current date : "+month+"/"+day+"/"+year+" Time: "+time);
		System.out.println("Card date : "+monthCard+"/"+dayCard+"/"+yearCard+" Time: "+timeCard);
		short[] tempsEcoule = {0,0,0,0};
		short dayOfMonth = 0;
		short diffDay = (short) (day - dayCard);
		byte diffMonth = (byte) (month - monthCard);
		short diffYear = (short) (year - yearCard);
		
		if(diffDay < 0) {
			if(monthCard == 4 || monthCard == 6 || monthCard == 9 || monthCard == 11) {
				dayOfMonth = 30;
			} else {
				if(monthCard == 2) {
					dayOfMonth = 29;
				} else {
					dayOfMonth = 31;
				}
			}
			diffDay = (short) (dayOfMonth + diffDay);
			//month--;
			diffMonth--;
		}
		
		if(diffMonth < 0) {
			diffMonth = (byte) (12 + diffMonth);
			//year--;
			diffYear--;
		}
		
		System.out.println("Diff month :"+diffMonth);
		System.out.println("Diff day :"+diffDay);
		System.out.println("Diff year :"+diffYear);
		
		if(diffDay > 22 || diffMonth > 0 || diffYear > 0) {
			tempsEcoule[0] = diffMonth;
			tempsEcoule[1] = diffDay;
			tempsEcoule[2] = diffYear;
			tempsEcoule[3] = 0;
			
			return (tempsEcoule);
			//return 32000; // Retourner year, month, day
		}
		
		short timeOfDay = (short) (diffDay*60*24);
		tempsEcoule[0] = 0;
		tempsEcoule[1] = 0;
		tempsEcoule[2] = 0;
		tempsEcoule[3] = (short) (time - timeCard + timeOfDay);
		//System.out.println("Temps ecoule en fonction :"+tempsEcoule);
		return tempsEcoule;
	}
	
	/**
	 * Fonction permettant d'acheter un voyage
	 * @param apdu - APDU
	 * @param year - année du dernier achat
	 * @param month - mois du dernier achat
	 * @param day - jour du dernier achat
	 * @param time - heure du dernier achat
	 * @param line - ligne du dernier bus emprunté
	 * @param direction - direction du dernier bus emprunté
	 */
	public void achatVoyage(APDU apdu, short year, byte month, short day, short time, byte line, boolean direction) {
		/*short res = tempsEcoule(year,month,day,time);
		if(res > DUREE_VALIDITE_VOYAGE) {
			if(balance > 0) {
				balance --;
				yearCurrent = year;
				monthCurrent = month;
				dayCurrent = day;
				timeCurrent = time;
				lineCurrent = line;
				directionCurrent = direction;
				apdu.setOutgoingAndSend((short) 0, (short)  1);
				//System.out.println("Le nouveau voyage est acheté");
			} else {
				ISOException.throwIt(SW_EXCEED_SOLDE_MINIMUM);
				//System.out.println("Solde insuffisant - Veuillez acheter un titre de transport auprès du conducteur");
			}
		} else {
			//System.out.println("Le voyage en cours est valide");
			if((lineCard == lineCurrent) && (directionCard == directionCurrent)) {
				ISOException.throwIt(SW_VALID_TRIP);
				//System.out.println("Carte déjà validée");
			} else {
				ISOException.throwIt((short) ((SW_TIME_TRIP_EXCEED)+res));
				//System.out.println("Nombre de minutes restantes avant la fin de la validité du voyage : "+res);
			}
		}*/
	}
	
	/**
	 * Fonction permettant de vérifier le code PIN
	 * @param apdu - APDU
	 * @param buffer - saisie du code PIN
	 */
	/*
	private void VerifyPIN(APDU apdu, byte[] buffer) {
		byte pin_nb = buffer[ISO7816.OFFSET_P1];
		if ((pin_nb < 0) || (pin_nb >= 4))
			ISOException.throwIt(SW_INCORRECT_P1);
		OwnerPIN pin = pins[pin_nb];
		if (pin == null)
			ISOException.throwIt(SW_INCORRECT_P1);
		if (buffer[ISO7816.OFFSET_P2] != 0x00)
			ISOException.throwIt(SW_INCORRECT_P2);
		short numBytes = Util.makeShort((byte) 0x00, buffer[ISO7816.OFFSET_LC]);
		/*
		 * Here I suppose the PIN code is small enough to enter in the buffer
		 * TODO: Verify the assumption and eventually adjust code to support
		 * reading PIN in multiple read()s
		 /
		if (numBytes != apdu.setIncomingAndReceive())
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		if (!CheckPINPolicy(buffer, ISO7816.OFFSET_CDATA, (byte) numBytes))
			ISOException.throwIt(SW_INVALID_PARAMETER);
		if (pin.getTriesRemaining() == (byte) 0x00)
			ISOException.throwIt(SW_IDENTITY_BLOCKED);
		if (!pin.check(buffer, (short) ISO7816.OFFSET_CDATA, (byte) numBytes)) {
			LogoutIdentity(pin_nb);
			ISOException.throwIt(SW_AUTH_FAILED);
		}
		// Actually register that PIN has been successfully verified.
		logged_ids |= (short) (0x0001 << pin_nb);
	}*/
	
	/**
	 * Fonction principale du processus
	 */
	public void process(APDU apdu) throws ISOException {
		byte[] buffer = apdu.getBuffer();
		
		if(this.selectingApplet()) return;
		if(buffer[ISO7816.OFFSET_CLA] != CLA_ABUSAPPLET) {
			ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
		}
		
		switch(buffer[ISO7816.OFFSET_INS]) {
			case INS_RECHARGEMENT: // Rechargement (avec PIN)
				rechargeCard(apdu, buffer);
				break;
			case INS_ACHAT: // Achat d'un voyage
				break;
			case INS_CONS_CREDIT: // Consultation du crédit de la carte
				buffer[0]  = balance;
			    apdu.setOutgoingAndSend((short) 0, (short) 1);
			    break;
			case INS_CONS_VOGAYE_VALIDE: // Interroger la carte (voyage en cours ou non)
			   validationVayage(apdu, buffer);
			   break;
			case INS_DEBLOQUER_PIN: // Débloquer code PIN (avec PUK) 
			   UnblockPIN(apdu, buffer);
			   break;
			case INS_CONS_ACTIVITE:
			   break;
			default: // Exception
		       ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);  
		}
	}
}