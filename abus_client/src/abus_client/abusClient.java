package abus_client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

import abus_applet.abusApplet;

import com.sun.javacard.apduio.Apdu;
import com.sun.javacard.apduio.CadT1Client;
import com.sun.javacard.apduio.CadTransportException;

public class abusClient {

	public static void main(String[] args) throws CadTransportException, IOException {
		// TODO Auto-generated method stub
		/* Connexion a la Javacard */
		CadT1Client cad;
		Socket sckCarte;
		try {
			sckCarte = new Socket("localhost", 9025);
			sckCarte.setTcpNoDelay(true);
			BufferedInputStream input = new BufferedInputStream(sckCarte.getInputStream());
			BufferedOutputStream output = new BufferedOutputStream(sckCarte.getOutputStream());
			cad = new CadT1Client(input, output);
			cad.powerUp();
		} catch(Exception e) {
			System.out.println("Erreur : impossible de se connecter a la Javacard");
			return;
		}
		
		/* Mise hors tension de la carte */
		try {
			cad.powerDown();
		} catch(Exception e) {
			System.out.println("Erreur lors de l'envoi de la commande Powerdown");
			return;
		}
		
		/* Sélection de l'applet */
		/* Cette phase de sélection n'est pas indispensable pour le simulateur JCWDE */
		Apdu apdu = new Apdu();
		apdu.command[Apdu.CLA] = 0x00;
		apdu.command[Apdu.INS] = (byte) 0xA4;
		apdu.command[Apdu.P1] = 0x04;
		apdu.command[Apdu.P2] = 0x00;
		byte[] appletAID = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x00, 0x00 };
		apdu.setDataIn(appletAID);
		cad.exchangeApdu(apdu);
		if (apdu.getStatus() != 0x9000) {
			System.out.println("Erreur lors de la sélection de l'applet");
			System.exit(1);
		}
		
		/* Menu principal */
		boolean fin = false;
		while (!fin) {
			System.out.println();
			System.out.println("Application cliente Javacard");
			System.out.println("----------------------------");
			System.out.println();
			System.out.println("1 - Interroger le compteur");
			System.out.println("2 - Inrementer le compteur");
			System.out.println("3 - Decrementer le compteur");
			System.out.println("4 - Reinitialiser le compteur");
			System.out.println("5 - Quitter");
			System.out.println();
			System.out.println("Votre choix ?");

			int choix = System.in.read();
			while (!(choix >= '1' && choix <= '5')) {
				choix = System.in.read();
			}

			apdu = new Apdu();
			apdu.command[Apdu.CLA] = abusApplet.CLA_ABUSAPPLET;
			apdu.command[Apdu.P1] = 0x00;
			apdu.command[Apdu.P2] = 0x00;
			switch (choix) {
				case '1':
					// A DECOMMENTER apdu.command[Apdu.INS] = aBuSApplet.INS_INTERROGER_COMPTEUR;
					cad.exchangeApdu(apdu);
					if (apdu.getStatus() != 0x9000) {
						System.out.println("Erreur : status word different de 0x9000");
					} else {
						System.out.println("Valeur du compteur : " + apdu.dataOut[0]);
					}
					break;

				case '2':
					// A DECOMMENTER apdu.command[Apdu.INS] = aBuSApplet.INS_INCREMENTER_COMPTEUR;
					cad.exchangeApdu(apdu);
					if (apdu.getStatus() != 0x9000) {
						System.out.println("Erreur : status word different de 0x9000");
					} else {
						System.out.println("OK");
					}
					break;

				case '3':
					// A DECOMMENTER apdu.command[Apdu.INS] = aBuSApplet.INS_DECREMENTER_COMPTEUR;
					cad.exchangeApdu(apdu);
					if (apdu.getStatus() != 0x9000) {
						System.out.println("Erreur : status word different de 0x9000");
					} else {
						System.out.println("OK");
					}
					break;

				case '4':
					// A DECOMMENTER apdu.command[Apdu.INS] = aBuSApplet.INS_INITIALISER_COMPTEUR;
					byte[] donnees = new byte[1];
					donnees[0] = 0;
					apdu.setDataIn(donnees);
					cad.exchangeApdu(apdu);
					if (apdu.getStatus() != 0x9000) {
						System.out.println("Erreur : status word different de 0x9000");
					} else {
						System.out.println("OK");
					}
					break;

				case '5':
					fin = true;
					break;
			}
		}	
	}
}
